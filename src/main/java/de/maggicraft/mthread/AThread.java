package de.maggicraft.mthread;

import de.maggicraft.mlog.MLog;
import org.jetbrains.annotations.NotNull;

/**
 * Führt solange Aufgaben des {@code AssemblyThread}s aus, bis alle seine Aufgaben ausgeführt
 * wurden. Gibt ggf. Statistiken über verwendete Zeit aus.
 * <p>
 * Created by Marc Schmidt on 06.01.2017.
 */
@Deprecated
public class AThread extends Thread {

   @NotNull
   private final AssemblyThread mMaster;
   private long mProcessingTime;

   /**
    * @param pMaster   AssemblyThread, der die Aufgaben verwaltet
    * @param pName     Name des Threads
    * @param pPriority Priorität des Threads
    */
   public AThread(@NotNull AssemblyThread pMaster, @NotNull String pName, int pPriority) {
      mMaster = pMaster;

      setName(pName);
      setPriority(pPriority);
   }

   /**
    * Führt solange Aufgaben des {@code AssemblyThread}s aus bis alle Aufgaben ausgeführt wurden.
    */
   @Override
   public void run() {
      int runId;
      while ((runId = mMaster.getNext()) != -1) {
         if (mMaster.showResults()) {
            long time = System.currentTimeMillis();
            if (mMaster.statsAll()) {
               MLog.log(getName() + ":\tstarted");
            }

            mMaster.run(runId);

            long timeEnd = System.currentTimeMillis() - time;
            mProcessingTime += timeEnd;

            if (mMaster.statsAll()) {
               MLog.log(getName() + ":\tended\t in " + ((double) timeEnd / 1000) + 's');
            }
         } else {
            mMaster.run(runId);
         }
      }

      mMaster.finished(this);
   }

   public long getProcessingTime() {
      return mProcessingTime;
   }

   @Override
   public String toString() {
      return "AThread{ master=" + mMaster + ", processingTime=" + mProcessingTime + '}';
   }
}
