package de.maggicraft.mthread;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * created on 2018.12.26 by marc
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author marc
 */
@Deprecated
public final class MConsumerBundle {

   @NotNull
   private final LinkedBlockingQueue<Runnable> mTasks = new LinkedBlockingQueue<>();
   private final Collection<MConsumer> mConsumers = new LinkedBlockingQueue<>();
   @NotNull
   private final AtomicBoolean mIsRunning = new AtomicBoolean(true);
   private final String mName;
   private final int mPriority;

   public MConsumerBundle(int pConsumerQuantity) {
      this(pConsumerQuantity, "consumer", Thread.NORM_PRIORITY);
   }

   public MConsumerBundle(int pConsumerQuantity, String pName, int pPriority) {
      if ((pPriority < Thread.MIN_PRIORITY) || (pPriority > Thread.MAX_PRIORITY)) {
         throw new IllegalArgumentException("priority not in bounds 1-10");
      }

      mPriority = pPriority;
      mName = pName;

      for (int i = 0; i < pConsumerQuantity; i++) {
         addConsumer(new MConsumer(mTasks, mName + i, mPriority));
      }
   }

   public void addConsumer() {
      addConsumer(new MConsumer(mTasks, mName, mPriority));
   }

   public void addConsumer(@NotNull MConsumer pConsumer) {
      mConsumers.add(pConsumer);
      if (mIsRunning.get()) {
         pConsumer.start();
      }
   }

   public void addTask(@NotNull Runnable pTask) {
      mTasks.add(pTask);
   }

   public void start() {
      mIsRunning.set(true);
      for (MConsumer consumer : mConsumers) {
         consumer.start();
      }
   }

   public void stop() {
      mIsRunning.set(false);
      for (MConsumer consumer : mConsumers) {
         consumer.stop();
      }
   }

   @Override
   public String toString() {
      return "MConsumerBundle{ isRunning=" + mIsRunning + ", name='" + mName + '\'' +
             ", priority=" + mPriority + '}';
   }
}
