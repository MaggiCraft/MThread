package de.maggicraft.mthread;

import de.maggicraft.mlog.MLog;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * created on 2018.12.26 by marc
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author marc
 */
@Deprecated
public class MConsumer {

   @NotNull
   private final AtomicBoolean mRunning = new AtomicBoolean(false);
   private final @NotNull BlockingQueue<? extends Runnable> mTasks;
   @NotNull
   private final Thread mThread;

   public MConsumer(@NotNull BlockingQueue<? extends Runnable> pTasks) {
      this(pTasks, null, Thread.NORM_PRIORITY);
   }

   public MConsumer(@NotNull BlockingQueue<? extends Runnable> pTasks, @Nullable String pName,
         int pPriority) {
      mTasks = pTasks;

      mThread = new Thread(() -> {
         while (mRunning.get()) {
            try {
               mTasks.take().run();
            } catch (InterruptedException pE) {
               MLog.log(pE);
            }
         }
      });

      mThread.setPriority(pPriority);
      if (pName != null) {
         mThread.setName(pName);
      }
   }

   public void start() {
      if (mRunning.compareAndSet(false, true)) {
         mThread.start();
      }
   }

   public void stop() {
      mRunning.set(false);
   }

   @Override
   public String toString() {
      return "MConsumer{ running=" + mRunning + ", thread=" + mThread + '}';
   }
}
