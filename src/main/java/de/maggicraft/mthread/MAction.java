package de.maggicraft.mthread;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Eine Aktion die von einem {@code MThread} ausgeführt wird und optional einen Namen hat.
 * <p>
 * Created by Marc Schmidt on 19.03.2017.
 */
@Deprecated
public class MAction {

   @NotNull
   private final Runnable mAction;
   @Nullable
   private String mName;

   public MAction(@NotNull Runnable pAction) {
      mAction = pAction;
   }

   public MAction(@NotNull Runnable pAction, @Nullable String pName) {
      mAction = pAction;
      mName = pName;
   }

   public void run() {
      mAction.run();
   }

   /**
    * Setzt den Namen, sofern dieser {@code null} ist.
    */
   public void setName(@Nullable String pName) {
      if (mName == null) {
         mName = pName;
      }
   }

   @Nullable
   public String getName() {
      return mName;
   }

   @Override
   public String toString() {
      return "MAction{ action=" + mAction + ", name='" + mName + '\'' + '}';
   }
}
