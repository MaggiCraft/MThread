package de.maggicraft.mthread;

import de.maggicraft.mlog.MLog;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Semaphore;

/**
 * Implements an lock which solves Marc's classification with problem level 4. An entered lock can
 * only entered once.
 * <p>
 * created on 2018.12.28 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
@Deprecated
public class MLock {

   @NotNull
   private final Semaphore mSemaphore = new Semaphore(1);
   private boolean mLeftOnce;

   /**
    * Locks the calling method and returns if the lock was left.
    *
    * @return {@code true} the lock was already left, {@code false} otherwise.
    */
   public boolean enter() {
      try {
         mSemaphore.acquire();
      } catch (InterruptedException pE) {
         MLog.log(pE);
      }
      return mLeftOnce;
   }

   /**
    * Leaves the lock
    */
   public void leave() {
      mLeftOnce = true;
      mSemaphore.release();
   }

   @Override
   public String toString() {
      return "MLock{ semaphore=" + mSemaphore + ", leftOnce=" + mLeftOnce + '}';
   }
}
