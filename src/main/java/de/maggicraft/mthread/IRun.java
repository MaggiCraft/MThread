package de.maggicraft.mthread;

/**
 * Repräsentiert eine Menge von Aufgaben, die einzeln durch eine {@code runId} ausgeführt werden
 * können.
 * <p>
 * Created by Marc Schmidt on 06.01.2017.
 */
@Deprecated
@FunctionalInterface
public interface IRun {

   void run(int pRunId);

}
