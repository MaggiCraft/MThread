package de.maggicraft.mthread;

import de.maggicraft.mlog.MLog;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Ein {@code AssemblyThread} verwaltet eine Menge von Aufgaben. Eine Aufgabe wird durch {@link
 * IRun} und die darin enthaltene Methode {@link IRun#run(int)} definiert. Diese Methode wird für
 * jeden Index von {@code [0,mTill]}, der Reihe nach, genau einmal ausgeführt.
 * <p>
 * Created by Marc Schmidt on 06.01.2017.
 */
@Deprecated
@SuppressWarnings("unused")
public class AssemblyThread {

   @NotNull
   private final IRun mRun;
   @NotNull
   private final String mName;
   private final int mTill;
   private final int mThrQuantity;
   private final int mPriority;
   @Nullable
   private Runnable mEnd;
   private double mProcessingTime;
   private long mTime;
   private int mCur;
   private int mFinished;
   private int mStats;

   /**
    * Erstellt einen {@code AssemblyThread} mit übergebenen {@link IRun} und Begrenzung durch {@code
    * pTill}.
    *
    * @param pRun         Methode die jeder Sub-Thread mit übergebener {@code runId} ausgeführt.
    * @param pTill        Begrenzung von {@link IRun}
    * @param pName        Name des AssemblyThreads von dem die Namen der {@code AThread} abgeleitet
    *                     werden
    * @param pThrQuantity Anzahl der Threads die die {@link IRun}s ausführen
    */
   public AssemblyThread(@NotNull IRun pRun, int pTill, @NotNull String pName, int pThrQuantity) {
      this(pRun, pTill, pName, pThrQuantity, 5, MasterThread.STATS_NONE);
   }

   /**
    * Erstellt einen {@code AssemblyThread} mit übergebenen {@link IRun} und Begrenzung durch {@code
    * pTill}.
    *
    * @param pRun         Methode die jeder Sub-Thread mit übergebener {@code runId} ausgeführt.
    * @param pTill        Begrenzung von {@link IRun}
    * @param pName        Name des AssemblyThreads von dem die Namen der {@code AThread} abgeleitet
    *                     werden
    * @param pThrQuantity Anzahl der Threads die die {@link IRun}s ausführen
    * @param pPriority    Priorität mit der jede von {@link IRun} definierte Aufgabe ausgeführt
    *                     wird
    * @param pStats       setzt inwiefern Stats ausgegeben werden
    */
   public AssemblyThread(@NotNull IRun pRun, int pTill, @NotNull String pName, int pThrQuantity,
         int pPriority, int pStats) {
      mRun = pRun;
      mName = pName;
      mTill = pTill;
      mThrQuantity = pThrQuantity;
      mPriority = pPriority;
      mStats = pStats;

      mCur = -1;
      mFinished = 0;
   }

   @Override
   public String toString() {
      return "AssemblyThread{ run=" + mRun + ", name='" + mName + '\'' + ", till=" + mTill +
             ", thrQuantity=" + mThrQuantity + ", priority=" + mPriority + '}';
   }

   /**
    * Startet alle {@code AThread} und weißt diesen Namen und Priorität zu.
    */
   @NotNull
   public AssemblyThread start() {
      mTime = System.currentTimeMillis();
      for (int i = 0; i < mThrQuantity; i++) {
         new AThread(this, mName + '-' + i, mPriority).start();
      }
      return this;
   }

   /**
    * Wenn alle {@code AThread}s beendet wurden, wird {@link AssemblyThread#mEnd#run(int)}
    * ausgeführt. Stats werden abhängig von {@link AssemblyThread#mStats} ausgegeben.
    *
    * @param pThread der beendet wurde
    */
   public synchronized void finished(@NotNull AThread pThread) {
      mFinished++;

      if (showResults()) {
         if (statsFinished()) {
            String threadName = pThread.getName();
            double processingTime = pThread.getProcessingTime() / 1000.0D;
            MLog.log(
                  mName + ":\tfinished [" + threadName + "] (" + mFinished + '/' + mThrQuantity +
                  ")\tprocessing time: " + processingTime);
         }
         mProcessingTime += pThread.getProcessingTime();
      }

      if (mFinished == mThrQuantity) {
         if (showResults()) {
            double timeEnd = System.currentTimeMillis() - mTime;
            double seconds = timeEnd / 1000;
            double totalTime = mProcessingTime / 1000;
            double factor = (double) (int) ((mProcessingTime / timeEnd) * 100) / 100;
            MLog.log("ended all threads in " + seconds + " seconds, total processing time: " +
                     totalTime + ", factor: " + factor);
         }
         if (mEnd != null) {
            mEnd.run();
         }
      }
   }

   /**
    * Führt eine Aufgabe mit übergebener {@code runId} aus.
    *
    * @param pRunId runId der Aufgabe
    */
   public void run(int pRunId) {
      mRun.run(pRunId);
   }

   /**
    * Gibt die jeweils nächste {@code runId} zurück.
    *
    * @return nächste {@code runId}
    */
   public int getNext() {
      int cur;
      synchronized (this) {
         mCur++;
         cur = mCur;
      }
      if (cur < mTill) {
         return cur;
      }
      return -1;
   }

   @NotNull
   public AssemblyThread endRun(Runnable pEnd) {
      mEnd = pEnd;
      return this;
   }

   /**
    * Setzt inwiefern Stats ausgegeben werden.
    *
    * @param pStats {@code MasterThread#STATS_NONE} keine Stats werden ausgegeben<br> {@code
    *               MasterThread#STATS_RESULTS} eine Zusammenfassung wird am Ende ausgegeben<br>
    *               {@code MasterThread#STATS_FINISHED} eine Zusammenfassung am Ende und von jedem
    *               beendeten {@code MThread} wird ausgegeben<br> {@code MasterThread#STATS_ALL}
    *               alle Stats werden ausgegeben<br>
    */
   @NotNull
   public AssemblyThread setStats(int pStats) {
      mStats = pStats;
      return this;
   }

   public boolean showResults() {
      return mStats >= MasterThread.STATS_RESULTS;
   }

   public boolean statsFinished() {
      return mStats >= MasterThread.STATS_FINISHED;
   }

   public boolean statsAll() {
      return mStats >= MasterThread.STATS_ALL;
   }
}
