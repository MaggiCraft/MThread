package de.maggicraft.mthread;

import de.maggicraft.mlog.MLog;
import org.jetbrains.annotations.NotNull;

/**
 * Führt solange Aktionen des {@link MasterThread}s aus, bis alle seine Aktionen ausgeführt wurden.
 * Gibt ggf. Statistiken über verwendete Zeit aus.
 * <p>
 * Created by Marc Schmidt on 19.03.2017.
 */
@Deprecated
public class MThread extends Thread {

   @NotNull
   private final MasterThread mMaster;
   private long mProcessingTime;

   /**
    * @param pMaster   MasterThread, der die Aktionen beinhaltetL
    * @param pName     Name des Threads
    * @param pPriority Priorität des Threads
    */
   public MThread(@NotNull MasterThread pMaster, @NotNull String pName, int pPriority) {
      mMaster = pMaster;

      setName(pName);
      setPriority(pPriority);
   }

   /**
    * Führt solange {@link MAction}s des {@link MasterThread}s aus bis alle Aktionen ausgeführt
    * wurden. Danach endet der Thread und {@link MasterThread#finished(MThread)} wird aufgerufen.
    * Stats mit der {@code Processing Time} werden abhängig von {@code MasterThread#mStats}
    * ausgegeben.
    */
   @Override
   public void run() {
      MAction action;
      while ((action = mMaster.getNext()) != null) {
         if (mMaster.statsResults()) {
            long time = System.currentTimeMillis();
            if (mMaster.statsAll()) {
               MLog.log(getName() + ":\tstarted");
            }

            action.run();

            long timeEnd = System.currentTimeMillis() - time;
            mProcessingTime += timeEnd;

            if (mMaster.statsAll()) {
               MLog.log(getName() + ":\tended in " + ((double) timeEnd / 1000) + 's');
            }
         } else {
            action.run();
         }
      }

      mMaster.finished(this);
   }

   public long getProcessingTime() {
      return mProcessingTime;
   }

   @Override
   public String toString() {
      return "MThread{ master=" + mMaster + ", processingTime=" + mProcessingTime + '}';
   }
}
