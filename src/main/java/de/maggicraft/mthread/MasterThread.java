package de.maggicraft.mthread;

import de.maggicraft.mlog.MLog;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Ein MasterThread verwaltet eine Anzahl von {@code MThread}s die {@link Runnable}s ausführen.
 * <p>
 * Created by Marc Schmidt on 19.03.2017.
 */
@SuppressWarnings("unused")
@Deprecated
public class MasterThread {

   public static final int STATS_NONE = -1;
   public static final int STATS_RESULTS = 0;
   public static final int STATS_FINISHED = 1;
   public static final int STATS_ALL = 2;
   @NotNull
   private final String mName;
   private final int mThrQuantity;
   private final int mPriority;
   @NotNull
   private List<MAction> mActions;
   /**
    * Aktion die am Ende ausgeführt wird
    */
   @Nullable
   private MAction mEndAction;
   private double mProcessingTime;
   private long mTime;
   private int mCur;
   private int mFinished;
   private int mStats;

   /**
    * Erstellt einen {@link MasterThread}, die {@link MAction} werden später mit {@link
    * MasterThread#add(Runnable)} oder {@link MasterThread#add(Runnable, String)} erstellt.
    *
    * @param pName        Name des MasterThreads von dem die Namen der {@code MThread} abgeleitet
    *                     werden
    * @param pThrQuantity Name des MasterThreads von dem die Namen der {@code MThread} abgeleitet
    *                     werden
    */
   public MasterThread(@NotNull String pName, int pThrQuantity) {
      this(new ArrayList<>(), pName, pThrQuantity, 5, STATS_NONE);
   }

   /**
    * Erstellt einen {@link MasterThread}, die {@link MAction} werden später mit {@link
    * MasterThread#add(Runnable)} oder {@link MasterThread#add(Runnable, String)} erstellt.
    *
    * @param pName        Name des MasterThreads von dem die Namen der {@code MThread} abgeleitet
    *                     werden
    * @param pThrQuantity Name des MasterThreads von dem die Namen der {@code MThread} abgeleitet
    *                     werden
    * @param pPriority    Priorität mit der jede {@link Runnable} ausgeführt wird
    * @param pStats       setzt inwiefern Stats ausgegeben werden
    */
   public MasterThread(@NotNull String pName, int pThrQuantity, int pPriority, int pStats) {
      this(new ArrayList<>(), pName, pThrQuantity, pPriority, pStats);
   }

   /**
    * Erstellt einen {@link MasterThread} und überprüft die übergebenen {@link Runnable}s auf {@code
    * null}.
    *
    * @param pActions     Aktionen die ausgeführt werden
    * @param pName        Name des MasterThreads von dem die Namen der {@code MThread} abgeleitet
    *                     werden
    * @param pThrQuantity Anzahl der Threads die die {@link Runnable}s ausführen
    */
   public MasterThread(@NotNull List<MAction> pActions, @NotNull String pName, int pThrQuantity) {
      this(pActions, pName, pThrQuantity, 5, STATS_NONE);
   }

   /**
    * Erstellt einen {@link MasterThread} und überprüft die übergebenen {@link Runnable}s auf {@code
    * null}.
    *
    * @param pActions     Aktionen die ausgeführt werden
    * @param pName        Name des MasterThreads von dem die Namen der {@code MThread} abgeleitet
    *                     werden
    * @param pThrQuantity Anzahl der Threads die die {@link Runnable}s ausführen
    * @param pPriority    Priorität mit der jede {@link Runnable} ausgeführt wird
    * @param pStats       setzt inwiefern Stats ausgegeben werden
    * @throws NullPointerException wenn eine übergebene {@link Runnable} {@code null} ist
    * @see Thread#setPriority(int)
    * @see MasterThread#setStats(int)
    */
   public MasterThread(@NotNull List<MAction> pActions, @NotNull String pName, int pThrQuantity,
         int pPriority, int pStats) {
      mActions = pActions;
      mName = pName;
      mThrQuantity = pThrQuantity;
      mPriority = pPriority;
      mStats = pStats;

      initValues();
   }

   private static double factor(double pProcessingTime, double pTimeEnd) {
      return (double) (int) ((pProcessingTime / pTimeEnd) * 100) / 100;
   }

   /**
    * Setzt die Werte zum Durchlaufen der Aktionen auf ihre Anfangswerte.
    */
   public void initValues() {
      mCur = -1;
      mFinished = 0;

      for (int i = 0; i < mActions.size(); i++) {
         if (mActions.get(i) == null) {
            throw new NullPointerException("action " + i + " is not initialized");
         }
      }
   }

   /**
    * Startet alle {@code MThread} und weißt diesen Namen und Priorität zu.
    */
   @NotNull
   public MasterThread start() {
      mTime = System.currentTimeMillis();
      for (int i = 0; i < mThrQuantity; i++) {
         new MThread(this, mName + '-' + i, mPriority).start();
      }
      return this;
   }

   /**
    * Wartet bis alle Threads zu ende gelaufen sind.
    */
   public void waitToEnd() {
      while (mFinished != mThrQuantity) {
         try {
            Thread.sleep(1000);
         } catch (InterruptedException pE) {
            MLog.log(pE);
         }
      }
   }

   /**
    * Wenn alle {@code MThread}s beendet wurden, wird {@link MasterThread#end()} ausgeführt. Stats
    * werden abhängig von {@link MasterThread#mStats} ausgegeben.
    *
    * @param pThread der beendet wurde
    */
   public synchronized void finished(@NotNull MThread pThread) {
      mFinished++;

      if (statsResults()) {
         if (statsFinished()) {
            String threadName = pThread.getName();
            double processingTime = pThread.getProcessingTime() / 1000.0D;
            MLog.log(mName + ":\tfinished [" + threadName + "] (" + mFinished + '/' + mThrQuantity +
                     ")\tprocessing time: " + processingTime);
         }
         mProcessingTime += pThread.getProcessingTime();
      }

      if (mFinished == mThrQuantity) {
         if (statsResults()) {
            double timeEnd = System.currentTimeMillis() - mTime;
            double seconds = timeEnd / 1000;
            double totalTime = mProcessingTime / 1000;
            double factor = (double) (int) ((mProcessingTime / timeEnd) * 100) / 100;
            MLog.log("ended all threads in " + seconds + " seconds, total processing time: " +
                     totalTime + ", factor: " + factor);
         }
         end();
      }
   }

   /**
    * Methode die ausgeführt wird, nachdem alle {@link Runnable}s ausgeführt wurden und alle {@code
    * MThread} beendet wurden.
    */
   public void end() {
      if (mEndAction != null) {
         mEndAction.run();
      }
   }

   /**
    * Fügt die übergebene {@link MAction} hinzu.
    *
    * @param pAction {@link Runnable} aus die der die {@link MAction} erstellt wird
    */
   public void add(@NotNull Runnable pAction) {
      mActions.add(new MAction(pAction));
   }

   /**
    * Fügt die übergebene {@link MAction} mit Namen hinzu.
    *
    * @param pAction {@link Runnable} aus die der die {@link MAction} erstellt wird
    * @param pName   Name der Aktion
    */
   public void add(@NotNull Runnable pAction, @NotNull String pName) {
      mActions.add(new MAction(pAction, pName));
   }

   public void endAction(@NotNull Runnable pAction) {
      mEndAction = new MAction(pAction);
   }

   /**
    * Gibt die jeweils nächste {@link Runnable} zurück.
    */
   @Nullable
   public MAction getNext() {
      int cur;
      synchronized (this) {
         mCur++;
         cur = mCur;
      }
      if (cur < mActions.size()) {
         mActions.get(cur).setName("action " + cur);
         return mActions.get(cur);
      }
      return null;
   }

   /**
    * Setzt inwiefern Stats ausgegeben werden.
    *
    * @param pStats {@link MasterThread#STATS_NONE} keine Stats werden ausgegeben<br> {@link
    *               MasterThread#STATS_RESULTS} eine Zusammenfassung wird am Ende ausgegeben<br>
    *               {@link MasterThread#STATS_FINISHED} eine Zusammenfassung am Ende und von jedem
    *               beendeten {@code MThread} wird ausgegeben<br> {@link MasterThread#STATS_ALL}
    *               alle Stats werden ausgegeben<br>
    */
   @NotNull
   public MasterThread setStats(int pStats) {
      mStats = pStats;
      return this;
   }

   public void setActions(@NotNull List<MAction> pActions) {
      mActions = pActions;
   }

   public boolean statsResults() {
      return mStats >= STATS_RESULTS;
   }

   public boolean statsFinished() {
      return mStats >= STATS_FINISHED;
   }

   public boolean statsAll() {
      return mStats >= STATS_ALL;
   }

   public int getFinished() {
      return mFinished;
   }
}
