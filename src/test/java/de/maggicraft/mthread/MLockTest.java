package de.maggicraft.mthread;

import de.maggicraft.mlog.MLog;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;

/**
 * created on 2018.12.28 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
class MLockTest {

   @Test
   void testLock() {
      MLock lock = new MLock();
      new Thread(() -> {
         lock.enter();
         try {
            Thread.sleep(3000);
         } catch (InterruptedException pE) {
            MLog.log(pE);
         }
         lock.leave();
      }).start();

      try {
         Thread.sleep(1000);
      } catch (InterruptedException pE) {
         MLog.log(pE);
      }

      Runnable run = () -> {
         long time = System.currentTimeMillis();
         lock.enter();
         try {
            Thread.sleep(1000);
         } catch (InterruptedException pE) {
            MLog.log(pE);
         }
         lock.leave();
         long current = System.currentTimeMillis();
         Assertions.assertTrue((current - time) > (2 * 1000));
         Assertions.assertTrue((current - time) < (5 * 1000));
      };

      new Thread(run).start();
      new Thread(run).start();
   }

   @Test
   void testMutualExclusion() {
      MLock lock = new MLock();

      Runnable runnable = () -> {
         try {
            if (lock.enter()) {
               return;
            }

            try {
               Thread.sleep(2000);
            } catch (InterruptedException pE) {
               MLog.log(pE);
            }
         } finally {
            lock.leave();
         }
      };

      runnable.run();
      runnable.run();

      Assertions.assertTimeoutPreemptively(Duration.ofSeconds(1), runnable::run);
   }
}
